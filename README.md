## Manim在算法可视化分析的应用

### 1. Manim

Mainm是一种开源的自定义动画工具（Grant Sanderson），Manim生成的动画主要通过Python的代码生成，比较适合创建一些时间较短的课程动画。Mainm是一种开源软件，Manim主要包括5个组件：Python、Latex、Cairo、FFmpeg、Sox。Mainm的主页是<https://www.manim.community>。

#### 1.1 数据结构中应用

- [x] 数组

  通过动画说明数组的遍历过程，数组的第一元素从0下标开始，最后一个元素是数组长度-1.

![](fig1/数组.gif)

- [x] 添加列表元素

  ArrayList可以动态的添加元素，新元素可以添加到已有元素的后部。

![](fig1/列表.gif)

- [x] 集合操作

  集合的元素不能重复，集合中添加元素的过程，重复值无法添加。

![](fig1/集合.gif)

#### 1.2 微积分

- [x] 导数、微分

![](fig1\导数.gif)

- [x] 切线、积分

![](fig1\积分.gif)

- [x] 圆锥面、椭圆锥面

![](fig1\圆锥面.gif)

#### 1.3 动画演示过程

- [x] 冒泡排序

![](fig1\冒泡排序.gif)

- [x] 矩阵运算

![](fig1\线性代数.gif)

- [x] 几何证明

![](fig1\几何证明.gif)

### 2 Manim的安装和开发环境的配置

#### 2.1 ubuntu20.04

- [x] 硬件（CPU-Intel i7-7700、显卡-GTX1060 3G）

- [x] Manim社区版

```bash
sudo apt update
sudo apt install libcairo2-dev libpango1.0-dev ffmpeg
sudo apt install python3-pip
pip3 install manim
```
- [x] VSCode的安装（code_1.66.2-1649664567_amd64.deb）$\color{red}{\textbf{Linux}下最好的开发环境}$

![](fig2\VSCode.png)

- [x] 新建项目，创建CreateCircle.py文件

```python
from manim import *

class CreateCircle(Scene):
    def construct(self):
        circle = Circle()
        circle.set_fill(PINK, opacity=0.5)
        self.play(Create(circle))
```
- [x] 编译程序，并创建CreateCircle.mp4文件

```bash
manim -pql scene.py CreateCircle
```
![](fig2\fig1.gif)

#### 附录：

[1] https://github.com/nbut-lxy/Manim

